<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VideoOptionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'autoplay' => '',
            'loop' => '',
            'controls' => '',
            'mute' => '',
            'fullscreen' => '',
            'frameborder' => '',
            'related_videos' => '',
            'gyroscope' => '',
            'accelerometer' => '',
            'picture' => '',
            'encrypt_media' => '',
            'start' => '',
            'portrait' => '',
            'title' => '',
            'byline' => '',
        ];
    }
}
