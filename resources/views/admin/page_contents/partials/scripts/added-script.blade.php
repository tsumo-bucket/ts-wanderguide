<script>
    var imageDataContainer = "";

    $(".submit-form-btn").click(function() {
    	var isValid = true;
    	var errorMessage = "";
    	
    	$.each($("form input[type=text]"), function() {
    		var _this = $(this);
    		if(!_this.val() && _this.prop("required")) {
                console.log(_this.attr("name"));
    			isValid = false;
    			if(_this.parents('.animated-placeholder').length > 0) {
    				_this.parents('.animated-placeholder').addClass("invalid");
    			}
				_this.addClass("invalid");
				errorMessage = "Please fill in all the required fields.";
    		}
    	});

    	if(!isValid) {
    		showNotifyToaster('error', '', errorMessage);
    	} else {
    		$(".form-parsley").submit();
    	}
    });

</script>